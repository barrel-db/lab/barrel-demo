# Demo application for Barrel-db

This repository demonstrates key features of the Barrel-db platform in an
as-close-as-possible realistic use case.

It is self-contained and allows user to deploy an application built over
Barrel-db using only [Docker](https://www.docker.com/) containers.

The barrel-platform official repo:

[https://gitlab.com/barrel-db/barrel-platform](https://gitlab.com/barrel-db/barrel-platform)

The elixir client:

[https://gitlab.com/barrel-db/barrel-ex](https://gitlab.com/barrel-db/barrel-ex)

## The online documentation

A documentation is available in [doc directory](doc).

You can view it online using [mkdocs](http://www.mkdocs.org/).
Just follow instruction provided by the [README](doc/README.md).

## Starting the docker machine

Build and start the docker containers:

    $ docker-compose up

Open your browser on provided web apps:

[http://localhost:4000/](http://localhost:4000/)

[http://localhost:4001/](http://localhost:4001/)
