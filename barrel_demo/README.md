# Barrel Demo

Umbrella application for barrel-demo micro-services applications:

* exchange: push transaction (transfert) into a barrel database
* wallet: listen to transaction arriving on a database, and create balance documents
* wallet_web: web application to display account's balances. Updated in real
  time with web sockets.
