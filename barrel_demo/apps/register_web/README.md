# Register Web

Web application to register wallet transactions.

## Parameters

| Environment variable | Description  |
| -------------------- | ------------ |
| `REGISTER_WEB_PORT`  |  HTTP port   |
| `EXCHANGE_DB`        |  URL of the barrel-db *exchange* database where transactions will be registered |
