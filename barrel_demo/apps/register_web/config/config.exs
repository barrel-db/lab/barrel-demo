use Mix.Config

config :register_web,
  port: System.get_env("REGISTER_WEB_PORT") || "4000",
  db: System.get_env("EXCHANGE_DB") || "http://localhost:7080/dbs/exchange"
