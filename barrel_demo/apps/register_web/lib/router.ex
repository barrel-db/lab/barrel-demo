defmodule Router do
  use Plug.Router

  plug Plug.Parsers, parsers: [:urlencoded]
  plug :match
  plug :dispatch

  get "/" do
    page_contents = EEx.eval_file("templates/index.eex", [])
    conn
    |> Plug.Conn.put_resp_content_type("text/html")
    |> Plug.Conn.send_resp(200, page_contents)
  end

  post "/register" do
    from = Map.get(conn.params, "from")
    to = Map.get(conn.params, "to")
    {amount,_} = Float.parse(Map.get(conn.params, "amount"))

    exchange_db_url = Application.get_env(:register_web, :db)
    {:ok, db} = Barrex.Database.open(exchange_db_url)
    Exchange.register(from, to, amount, db)

    page_contents = EEx.eval_file("templates/register.eex",
      [from: from, to: to, amount: amount])
    conn
    |> Plug.Conn.put_resp_content_type("text/html")
    |> Plug.Conn.send_resp(200, page_contents)
  end

  match _ do
    send_resp(conn, 404, "Page not found")
  end

end
