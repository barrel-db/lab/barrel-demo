defmodule RegisterWeb.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    port = String.to_integer(Application.get_env(:register_web, :port))
    children = [
      Plug.Adapters.Cowboy.child_spec(:http, Router, [], [
            dispatch: dispatch(), port: port
          ])
    ]

    opts = [strategy: :one_for_one, name: RegisterWeb.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp dispatch() do
    [
      {:_, [
          {:_, Plug.Adapters.Cowboy.Handler, {Router, []}}
        ]}
    ]
  end
end
