defmodule RegisterWebTest do
  use ExUnit.Case, async: true
  use Plug.Test

  @opts Router.init([])

  test "diplay form" do
    # Create a test connection
    conn = conn(:get, "/")

    # Invoke the plug
    conn = Router.call(conn, @opts)

    # Assert the response and status
    assert conn.state == :sent
    assert conn.status == 200
    assert String.contains?(conn.resp_body, "Register a new transfert")
  end
end
