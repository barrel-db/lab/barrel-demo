defmodule RegisterWeb.Mixfile do
  use Mix.Project

  def project do
    [app: :register_web,
     version: "0.1.0",
     build_path: "../../_build",
     config_path: "../../config/config.exs",
     deps_path: "../../deps",
     lockfile: "../../mix.lock",
     elixir: "~> 1.4",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  def application do
    [applications: [:logger, :httpoison, :cowboy, :plug, :barrex, :exchange],
     mod: {RegisterWeb.Application, []}]
  end

  defp deps do
    [{:cowboy, "~> 1.0.0"},
     {:plug, "~> 1.0"},
     {:barrex, git: "https://gitlab.com/barrel-db/barrel-ex.git", branch: "master"},
     {:exchange, in_umbrella: true}]
  end
end
