defmodule Wallet do

  require Logger

  def attach(exchange_db, wallet_db) do
    since = last_seq(wallet_db)
    Exchange.changes(exchange_db, since)
    |> Wallet.feed(wallet_db)
    |> Stream.run()
  end

  def feed(stream, wallet_db) do
    stream
    |> Stream.map(fn(change) -> log(change) end)
    |> Stream.map(fn(change) -> add(change["doc"], change["meta"]["seq"], wallet_db) end)
  end

  defp log(change) do
    transaction = change["doc"]
    from_id = transaction["from_account"]
    to_id = transaction["to_account"]
    amount = transaction["amount"]
    Logger.info to_string :io_lib.format(
      "received transaction from ~s to ~s for ~p",
      [from_id, to_id, amount])
    change
  end

  def add(transaction, db) do
    add(transaction, 0, db)
  end

  def add(transaction, seq, db) do
    from_id = transaction["from_account"]
    to_id = transaction["to_account"]
    amount = transaction["amount"]
    write_last_seq(seq, db)

    unless amount <= 0 do
      adjust(from_id, -amount, db)
      adjust(to_id, amount, db)
    end

  end

  defp adjust(account_id, amount, db) do
    account = case Barrex.Database.get(account_id, db) do
                {:error, :not_found} ->
                  doc = %{ "id" => account_id, "balance" => 0}
                  {:ok, _, _} = Barrex.Database.post(doc, db)
                  doc
                {:ok, doc, _} ->
                  doc
              end
    account_previous_balance = account["balance"]
    account_new_balance = account_previous_balance + amount
    account2 = %{ account | "balance" => account_new_balance }
    {:ok, _, _} = Barrex.Database.put(account2 ,db)
  end

  def balance(account_nr, db) do
    {:ok, doc, _} = Barrex.Database.get(account_nr, db)
    Map.get(doc, "balance")
  end

  def last_seq(db) do
    case Barrex.Database.get("lastseq", db) do
      {:ok, doc, _} -> doc["seq"]
      _ -> 0
    end
  end

  defp write_last_seq(last_seq, db) do
    doc = %{"id" => "lastseq", "seq" => last_seq}
    case Barrex.Database.put(doc, db) do
      {:ok, _, _} -> :ok
      {:error, :not_found} ->
        {:ok, _, _} = Barrex.Database.post(doc, db)
        :ok
      error ->
        error
    end
  end

end
