defmodule Wallet.Application do
  use Application
  require Logger

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    Logger.info "starting OTP application wallet "

    exchange_db_url = Application.get_env(:wallet, :exchange_db, "")
    wallet_db_url = Application.get_env(:wallet, :wallet_db, "")
    children = [worker(Wallet.Worker, [exchange_db_url, wallet_db_url])]

    opts = [strategy: :one_for_one, name: Wallet.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
