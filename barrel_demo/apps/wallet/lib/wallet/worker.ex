defmodule Wallet.Worker do

  use GenServer
  require Logger

  def start_link(exchange_db_url, wallet_db_url) do
    Logger.info "#{__MODULE__} starting..."
    Logger.info "#{__MODULE__} exchange db = #{exchange_db_url}"
    Logger.info "#{__MODULE__} wallet db   = #{wallet_db_url}"

    case try_database_open([exchange_db_url]) do
      {:ok, exchange_db}  ->
        case try_database_open([wallet_db_url]) do
          {:ok, wallet_db}  ->
            GenServer.start_link(__MODULE__, [exchange_db, wallet_db], [])
          error ->
            {:error, error}
        end
      error ->
        {:error, error}
    end
  end

  # try to open databases

  def try_database_open([]) do
    Logger.error "#{__MODULE__} unable to connect to any databases"
    {:error, :no_working_db}
  end

  def try_database_open([url|tail]) do
    Logger.info "#{__MODULE__} try to connect to #{url}"
    try do
      Barrex.Database.open(url)
    rescue
      error ->
        Logger.error "#{__MODULE__} failed to connect to #{url}: #{inspect error}"
        try_database_open(tail)
    end
  end

  # GenServer callbacks

  def init([exchange_db, wallet_db]) do
    Process.flag(:trap_exit, true)
    spawn_link fn -> Wallet.attach(exchange_db, wallet_db) end
    Logger.info "#{__MODULE__} started"
    {:ok, %{}}
  end

  def handle_info({:EXIT, _, _}, state) do
    Logger.error "#{__MODULE__} received EXIT signal"
    {:stop, :received_exit, state}
  end

  def terminate(reason, _status) do
    Logger.info "#{__MODULE__} terminating because #{inspect reason}"
    :ok
  end
end
