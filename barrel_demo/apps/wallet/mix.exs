defmodule Wallet.Mixfile do
  use Mix.Project

  def project do
    [app: :wallet,
     version: "0.1.0",
     build_path: "../../_build",
     config_path: "../../config/config.exs",
     deps_path: "../../deps",
     lockfile: "../../mix.lock",
     elixir: "~> 1.4",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  def application do
    [extra_applications: [:logger],
     mod: {Wallet.Application, []}]
  end

  defp deps do
    [{:exchange, in_umbrella: true},
     {:barrex, git: "https://gitlab.com/barrel-db/barrel-ex.git", branch: "master"},
     {:mix_test_watch, "~> 0.3", only: :dev, runtime: false}]
  end
end
