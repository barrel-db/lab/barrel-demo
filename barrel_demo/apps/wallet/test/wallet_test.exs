defmodule WalletTest do

  use ExUnit.Case

  describe "Wallet" do

    setup do
      {:ok, _} = Application.ensure_all_started(:barrex)
      [db: setup_db("wallet"), db_exchange: setup_db("exchange") ]
    end

    test "updates both accounts' balance with transaction's amount", context do
      db = context[:db]

      Wallet.add( %{ "from_account" => "123" , "to_account" => "2345" , "amount" => 42.0 } , db)

      assert Wallet.balance("123", db) == -42.0
      assert Wallet.balance("2345", db) == 42.0
    end

    test "don't update accounts' balance given transaction amount is negative", context do
      db = context[:db]
      Wallet.add(%{"from_account" => "123", "to_account" => "2345", "amount" => 42.0 }, db)

      negative_transfer =
        %{"from_account" => "123", "to_account" => "2345", "amount" => -42.0 }
      Wallet.add(negative_transfer, db)

      assert Wallet.balance("123", db) == -42
      assert Wallet.balance("2345", db) == 42
    end

    test "listens to transactions feed given an exchange db", context do
      db_exchange = context[:db_exchange]
      db_wallet = context[:db]
      {:ok, _, _} = Exchange.register(%{"from_account" => "123", "to_account" => "2345", "amount" => 42.0 }, db_exchange)
 
      transactions_stream = Exchange.changes(db_exchange)

      transactions_stream
      |> Stream.take(1)
      |> Wallet.feed(db_wallet)
      |> Stream.run

      assert Wallet.balance("123", db_wallet) == -42
    end

    test "store seq of change", context do
      db_exchange = context[:db_exchange]
      Wallet.add(%{"from_account" => "123",
                   "to_account" => "2345" ,
                   "amount" => 42.0 }, 42, db_exchange)

      assert 42 == Wallet.last_seq(db_exchange)
    end

  end

  defp setup_db(db_name) do
    :barrel_httpc.delete_database(db_url(db_name))
    :barrel_httpc.create_database(db_url(db_name))
    {:ok, db} = Barrex.Database.open(db_url(db_name))
    db
  end

  defp db_url(db_name) do
    case System.get_env("BARREL_URL") do
      :nil -> "http://127.0.0.1:7080/dbs/" <> db_name
      url -> url <> "/dbs/" <> db_name
    end
  end

end
