use Mix.Config

config :wallet,
  app: :wallet,
  exchange_db: System.get_env("EXCHANGE_DB") || "http://localhost:7080/dbs/exchange",
  wallet_db: System.get_env("WALLET_DB") || "http://localhost:7080/dbs/wallet"
