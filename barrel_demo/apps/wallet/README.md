# Wallet

Micro service to compute account balance from registered transactions.

## Parameters

| Environment variable | Description  |
| -------------------- | ------------ |
| `EXCHANGE_DB`        |  URL of the barrel-db *exchange* database where transactions have been registered |
| `WALLET_DB`          |  URL of the barrel-db *wallet* database where accounts will be stored |
