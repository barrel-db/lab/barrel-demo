# Wallet Web

Web application to display wallet balances.

## Parameters

| Environment variable | Description  |
| -------------------- | ------------ |
| `WALLET_WEB_PORT`    |  HTTP port   |
| `WALLET_DB`          |  URL of the barrel-db *wallet* database where account's balances have been stored |

