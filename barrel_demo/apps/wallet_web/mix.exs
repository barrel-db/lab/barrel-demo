defmodule WalletWeb.Mixfile do
  use Mix.Project

  def project do
    [app: :wallet_web,
     version: "0.1.0",
     build_path: "../../_build",
     config_path: "../../config/config.exs",
     deps_path: "../../deps",
     lockfile: "../../mix.lock",
     elixir: "~> 1.4",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  def application do
    [applications: [:logger, :cowboy, :plug, :barrex],
     mod: {WalletWeb, []}]
  end

  defp deps do
    [{:cowboy, "~> 1.0.0"},
     {:plug, "~> 1.0"},
     {:poison, "~> 3.1"},
     {:barrex, git: "https://gitlab.com/barrel-db/barrel-ex.git", branch: "master"}]
  end
end
