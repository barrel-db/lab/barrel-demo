defmodule SocketAccountHandler do
  require Logger

  @behaviour :cowboy_websocket_handler

  def init(_, _req, _opts) do
    {:upgrade, :protocol, :cowboy_websocket}
  end

  def websocket_init(_type, req, [wallet_db_url]) do
    {:ok, wallet_db} = Barrex.Database.open(wallet_db_url)
    parent = self()
    {account_id, _} = :cowboy_req.binding(:id, req)
    spawn_link fn -> send_changes(wallet_db, account_id, parent) end
    Logger.info "#{__MODULE__} started for account #{account_id}"
    {:ok, req, []}
  end

  defp send_changes(db, account_id, pid) do
    stream = Barrex.Database.changes(db)
    stream
    |> Stream.filter(fn(doc) -> Map.get(doc, "id") == account_id end)
    |> Stream.map(fn(doc) -> send pid, doc end)
    |> Stream.run()
  end

  def websocket_handle({:text, "ping"}, req, state) do
    {:reply, {:text, "pong"}, req, state}
  end

  def websocket_handle(_, req, state) do
    {:ok, req, state}
  end

  def websocket_info({:EXIT, _, _}, state) do
    Logger.error "#{__MODULE__} received EXIT signal"
    {:stop, :received_exit, state}
  end

  def websocket_info(doc, req, state) when is_map(doc) do
    json = Poison.encode!(doc)
    {:reply, {:text, json}, req, state}
  end

  def websocket_info(message, req, state) do
    {:reply, {:text, message}, req, state}
  end

  def websocket_terminate(_reason, _req, _state) do
    Logger.info "#{__MODULE__} terminate"
    :ok
  end
end
