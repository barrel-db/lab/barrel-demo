defmodule WalletWeb do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false
    port = String.to_integer(Application.get_env(:wallet_web, :port))
    wallet_db_url = Application.get_env(:wallet_web, :db)
    children = [
      Plug.Adapters.Cowboy.child_spec(:http, Router, [], [
            dispatch: dispatch(wallet_db_url), port: port
          ])
    ]

    opts = [strategy: :one_for_one, name: WalletWeb.Supervisor]
    Supervisor.start_link(children, opts)
  end

  defp dispatch(wallet_db_url) do
    [
      {:_, [
          {"/echo/ws", SocketHandler, [wallet_db_url]},
          {"/account/:id/ws", SocketAccountHandler, [wallet_db_url]},
          {:_, Plug.Adapters.Cowboy.Handler, {Router, []}}
        ]}
    ]
  end
end
