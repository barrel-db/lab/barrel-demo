defmodule Router do
  use Plug.Router

  plug Plug.Static, at: "/static", from: :wallet_web

  plug :match
  plug :dispatch


  get "/" do
    wallet_db_url = Application.get_env(:wallet_web, :db)
    {:ok, wallet_db} = Barrex.Database.open(wallet_db_url)
    {:ok, docs} = Barrex.Database.docs(wallet_db)
    ids = docs
    |> Enum.filter(fn(doc) -> doc["id"] != "lastseq" end)
    |> Enum.map(fn(doc) -> doc["id"] end)

    page_contents = EEx.eval_file("templates/index.eex", [ids: ids])
    conn
    |> Plug.Conn.put_resp_content_type("text/html")
    |> Plug.Conn.send_resp(200, page_contents)
  end

  get "/account/:id" do
    page_contents = EEx.eval_file("templates/account.eex", [id: id])
    conn
    |> Plug.Conn.put_resp_content_type("text/html")
    |> Plug.Conn.send_resp(200, page_contents)
  end

  get "/echo" do
    page_contents = EEx.eval_file("templates/echo.eex", [message: "world"])
    conn
    |> Plug.Conn.put_resp_content_type("text/html")
    |> Plug.Conn.send_resp(200, page_contents)
  end

  match _ do
    send_resp(conn, 404, "Page not found")
  end

end
