use Mix.Config

config :wallet_web,
  port: System.get_env("WALLET_WEB_PORT") || "4001",
  db: System.get_env("WALLET_DB") || "http://localhost:7080/dbs/wallet"
