defmodule Exchange do

  def register(from, to, amount, db) when is_float(amount) do
    command = %{"from_account" => from, "to_account" => to, "amount" => amount}
    register(command, db)
  end

  def register(command, db) do
    Barrex.Database.post(command, db)
  end

  def changes(db) do
    changes(db, 0)
  end

  def changes(db, last_seq) do
    Barrex.Database.changes(db, [since: last_seq, meta: true])
  end
end
