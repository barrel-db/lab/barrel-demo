defmodule Mix.Tasks.Exchange.Replicate do
  use Mix.Task
  require Logger

  @shortdoc "Instruct databases to replicate in a mesh"

  def run(args) do
    parsed = OptionParser.parse(args, switches: [
          server: :string
        ])

    case parsed do
	    {[server: server_url], args, _} ->

        mesh = for node <- args,
          other <- args,
          node != other do
            { node, other }
        end

        # replicate each pair in mesh
        unless length(mesh) <= 1 do
          Enum.map(mesh, fn {from, to} -> replicate(server_url, from, to) end)
        end

	    _ ->
        Logger.error "usage: mix Exchange.Replicate --server <url of server triggering replication> database_url (database_url)+"
    end
  end

  def replicate(server_url, from_url, to_url) do
    HTTPoison.start

    headers = [{"Content-Type", "application/json"}]
    url = "#{server_url}/replicate"
    body = Poison.encode!(%{:source => from_url, :target => to_url})

    resp = HTTPoison.post url, body , headers

    case resp do
      {:ok, %HTTPoison.Response{status_code: 200}} ->
        Logger.info "replication started from #{from_url} to #{to_url}"
      {:ok, %HTTPoison.Response{status_code: 400, body: body}} ->
        Logger.warn "error starting replication from #{from_url} to #{to_url}: #{body}"
      {:error, %HTTPoison.Error{reason: :econnrefused}} ->
        Logger.error "unable to reach barrel-db server at " <> server_url
    end
  end
end
