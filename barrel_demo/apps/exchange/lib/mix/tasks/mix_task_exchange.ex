defmodule Mix.Tasks.Exchange.Register do
  use Mix.Task
  require Logger
  require Mix.Config

  @shortdoc "Register a transfert between accounts"

  def run(args) do
    parsed = OptionParser.parse(args, switches: [
          db: :string,
          from: :string,
          to: :string,
          amount: :float
        ])
    case parsed do
	    {[db: db, from: from, to: to, amount: amount], _, _} ->
        register(db, from, to, amount)
	    _ ->
        Logger.error "usage: mix Exchange.Register --db <url of the exchange database> --from <source acount nb> --to <target accound nbt> --amount <amount to transfert>"
    end
  end

  def register(db_url, from, to, amount) do
    {:ok, _} = Application.ensure_all_started(:barrex)
    {:ok, db_exchange} = Barrex.Database.open(db_url)
    {:ok, _, _} = Exchange.register(
      %{"from_account" => from,
        "to_account" => to,
        "amount" => amount },
      db_exchange)
    Logger.info "exchange registered transfert from " <> from <> " to " <> to
  end
end
