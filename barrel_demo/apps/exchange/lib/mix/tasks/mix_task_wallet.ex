defmodule Mix.Tasks.Exchange.CreateDb do
  use Mix.Task
  require Logger

  @shortdoc "Create wallet and exchange databases"

  def run(args) do
    parsed = OptionParser.parse(args, switches: [
          server: :string,
          exchange: :boolean,
          wallet: :boolean
        ])

    case parsed do
	    {options, _, _} ->
        server_url = Keyword.get(options, :server)

        if true == Keyword.get(options, :exchange) do
            create_db(server_url, "exchange")
        end
        if true == Keyword.get(options, :wallet) do
            create_db(server_url, "wallet")
        end

	    _ ->
        Logger.error "usage: mix Exchange.CreateDb --server <url of the database to create> [--exchange|--wallet]"
    end
  end

  def create_db(barrel_url, db) do
    HTTPoison.start

    headers = [{"Content-Type", "application/json"}]
    url = barrel_url <> "/dbs"
    body = "{\"database_id\": \"" <> db <> "\"}"

    resp = HTTPoison.post url, body , headers

    case resp do
      {:ok, %HTTPoison.Response{status_code: 201}} ->
        Logger.info db <> " database created"
      {:ok, %HTTPoison.Response{status_code: 409}} ->
        Logger.warn db <> " database already exist"
      {:error, %HTTPoison.Error{reason: :econnrefused}} ->
        Logger.error "unable to reach barrel-db server at " <> barrel_url
    end
  end
end
