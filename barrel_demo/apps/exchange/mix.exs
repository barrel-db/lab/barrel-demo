defmodule Exchange.Mixfile do
  use Mix.Project

  def project do
    [app: :exchange,
     version: "0.1.0",
     build_path: "../../_build",
     config_path: "../../config/config.exs",
     deps_path: "../../deps",
     lockfile: "../../mix.lock",
     elixir: "~> 1.4",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  def application do
    [extra_applications: [:logger]]
  end

  defp deps do
    [{:barrex, git: "https://gitlab.com/barrel-db/barrel-ex.git", branch: "master"},
     {:poison, "~> 3.1"},
     {:httpoison, "~> 0.11.1"}]
  end
end
