# Demo Wallet

1) Start a barrel database.

    $ cd barrel-platform
    $ rebar3 shell

2) Open a terminal then start the wallet microservices

    $ cd barrel_demo/apps/wallet
    $ iex -S mix

You might need to create both `exchange` and `wallet` databases.

3) Open another terminal to listen to `wallet` database events:

Might be in the "simple" application shell.

```elixir
{:ok, db} = Barrex.Database.open("http://localhost:7080/dbs/wallet")
stream = Barrex.Database.changes(db)
stream |> Stream.map(fn(doc) -> IO.inspect(doc) end) |> Stream.run
```

4) Create some transactions in first terminal (Wallet micro-services):

```elixir
{:ok, exchange} = Barrex.Database.open("http://localhost:7080/dbs/exchange")

Exchange.register("abc","def", 42.0, exchange)
```
