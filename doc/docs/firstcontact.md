# Live Demo!

Start a barrel node on a clean (empty) datbases:

     $ cd barrel-platform
     $ rm -rf data
     $ rebar3 shell

## Document oriented REST API

Open a browser on the swagger page:

[http://localhost:7080/api-doc](http://localhost:7080/api-doc)

## Reactive processing

In simple app directory:

    $ iex -S mix

Then copy and paste:

```elixir
{:ok, db} = Barrex.Database.open("http://localhost:7080/dbs/testdb")
stream = Barrex.Database.changes(db)

stream |> Stream.map(fn(doc) -> IO.inspect(doc) end) |> Stream.run
```
