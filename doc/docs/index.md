# Welcome to Barrel-db!

This document is designed to be shown during the live demonstration. You should
keep it displayed on your screen while your are working in terminals and
web-page for demo.

## Key features

* Document oriented database
* Continuous change feed, enabling reactive applications
* Replication between databases


