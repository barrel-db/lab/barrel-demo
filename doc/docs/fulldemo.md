# Micro-services in docker containers

![Archi](archi.png)

Start the containers

     docker-compose up

Open your browser

[http://localhost:4000/](http://localhost:4000/)

[http://localhost:4001/](http://localhost:4001/)

## Cheat sheet docker

List all containers:

    docker ps -a

Remove a container:

    docker rm <id>

Remove old containers by age:

    docker ps --filter "status=exited" | grep '37 hours ago' | awk '{print $1}' | xargs --no-run-if-empty docker rm
