# Reactive processing

From [Reactive Manifesto](www.reactivemanifesto.org/):

* **Responsive**: focus on rapid and consitent response time
* **Resilient**: stays responsive in the face of failure
* **Elastic**: stays reponsive under varying workload
* **Message Driven**: rely on asynchronous message passing

# Micro-services everywhere!

![microservices](microservices.png)

