# Reactive API in Barrel-db

## The erlang way

Spawn a process that will send change as erlang messages:

```erlang
Self = self(),
Callback =
  fun(Change) ->
    Self ! {change, Change}
  end,
Options = #{since => 0, mode => sse, changes_cb => Callback },
{ok, Pid} = barrel_httpc_changes:start_link(Db, Options),
```

Receives the reactive feed as a flow of messages:

```erlang
collect_changes(0, Acc) ->
  lists:reverse(Acc).
collect_changes(I, Acc) ->
  receive
    {change, Change} ->
      collect_changes(I-1, [Change|Acc])
  after 500 ->
      collect_changes(0, [{error, timeout}|Acc])
  end.
```

## The elixir way

```elixir
{:ok, db} = Barrex.Database.open("http://localhost:7080/dbs/testdb")
stream = Barrex.Database.changes(db)

stream
|> Stream.take(3)
|> Stream.map(fn(doc) -> IO.inspect(doc) end)
|> Stream.run
```
