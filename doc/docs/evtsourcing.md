# Wallet micro-services

**Event-sourcing for wallet/banking transactions.**

![wallet](wallet.png)

1) We receive a transaction transfering money between two accounts

```elixir
%{"from_account" => "123", "to_account" => "456", "amount" => 42.0}
```

2) We *adjust* the balance of both accounts

```elixir
  def add(transaction, seq, db) do
    from_id = transaction["from_account"]
    to_id = transaction["to_account"]
    amount = transaction["amount"]
    write_last_seq(seq, db)

    unless amount <= 0 do
      adjust(from_id, -amount, db)
      adjust(to_id, amount, db)
    end

  end
```

3) We store the result in one document for each account:

```elixir
%{"id" => "123", "balance" => 1234.0}
```

```elixir
  defp adjust(account_id, amount, db) do
    account = case Barrex.Database.get(account_id, db) do
                {:error, :not_found} ->
                  doc = %{ "id" => account_id, "balance" => 0}
                  {:ok, _, _} = Barrex.Database.post(doc, db)
                  doc
                {:ok, doc, _} ->
                  doc
              end
    account_previous_balance = account["balance"]
    account_new_balance = account_previous_balance + amount
    account2 = %{ account | "balance" => account_new_balance }
    {:ok, _, _} = Barrex.Database.put(account2 ,db)
  end
```
