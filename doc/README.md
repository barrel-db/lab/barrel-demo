#  documentation

## Install mkdocs

You will need [Python](https://www.python.org/) and [virtualenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/)

    $ virtualenv env
    $ . env/bin/activate
    $ pip install -r requirements.txt

## Render the web site

    $ mkdocs serve

Then open http://localhost:8000

Live serve the site for external access:

    $ mkdocs serve -a 0.0.0.0:8000

## Build the site

    $ mkdocs build
